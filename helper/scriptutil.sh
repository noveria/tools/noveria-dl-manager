ACTIONLISTFILE="/tmp/actionlist"
debuglevel=3

#
# description: Logs the message with the correct prefix into the console
# usage: log <type> <message>
#
function log() {
    local prefix

    type=$1
    print=false
    shift
    message=$@
    case $type in
        "s")
            print=true
            prefix="%F{green}[SUCCESS]%f"
            ;;
        "d")
            if [[ "$debuglevel" -ge 4 ]]; then
                print=true
            fi
            prefix="%F{magenta}[DEBUG]%f"
            ;;
        "i")
            if [[ "$debuglevel" -ge 1 ]]; then
                print=true
            fi
            prefix="%F{cyan}[INFO]%f"
            ;;
        "w")
            if [[ "$debuglevel" -ge 2 ]]; then
                print=true
            fi
            prefix="%F{yellow}[WARN]%f"
            ;;
        "e")
            if [[ "$debuglevel" -ge 3 ]]; then
                print=true
            fi
            prefix="%F{red}[ERR]%f"
            ;;
    esac
    "$quiet" && print=false
    "$print" && print -P "$prefix $message"
}

#
# description: executes command(s), while checking if the command(s) exist on the system!
# usage: executeCommand "command1; command2; command3; ..."
#
function executeCommand() {
    command=$1

    command_array=("${(s/;/)command}")
    for cmd in ${command_array[@]}; do
        cmd=${cmd# }
        if ! command -v $(echo $cmd | awk '{print $1}') &> /dev/null; then
            log e "$(echo $cmd | awk '{print $1}') could not be found on this system!"
            exit 1
        fi
    done
    
    log d "Executing $command"
    "$dryrun" && log i "Would've executed: '$command'" || eval "$command"
    if [[ "$dryrun" = false && $? -eq 0 ]]; then
        log s "Successfully executed: $command"
    fi
}

#
# description: adds completed actions to /var/actionlist, for skipping actions, if script fails.
# usage: addAction actionName
#
function addAction() {
    log d "Adding action $1 to $ACTIONLISTFILE"
    echo "$1" | tee -a "$ACTIONLISTFILE" &> /dev/null
}

#
# description: checks in /var/actionlist, if the specified action in $1 has already been executed.
# If yes, the action will be skipped, if no, the action will be processed as normal.
# usage: checkAction actionName
#
function checkAction() {
    log d "Checking if '$1' has been already executed"
    if [ ! -z "$(cat $ACTIONLISTFILE 2>/dev/null | grep $1 | head -n1)" ]; then
        log i "Skipping Action '$1'"
        return 1
    fi
    return 0
}