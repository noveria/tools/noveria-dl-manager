# Noveria-DL-Manager - Minecraft Server Download Repository Manager
![](https://img.shields.io/badge/version-0.0.1-blue)
![](https://img.shields.io/badge/license-AGPLv3-blue)
```
 _   _                     _             ____  _     
| \ | | _____   _____ _ __(_) __ _      |  _ \| |    
|  \| |/ _ \ \ / / _ \ '__| |/ _` |_____| | | | |    
| |\  | (_) \ V /  __/ |  | | (_| |_____| |_| | |___ 
|_| \_|\___/ \_/ \___|_|  |_|\__,_|     |____/|_____|

Noveria-DL-Manager v0.0.1
Copyright (C) 2023 Noveria Network

This program may be freely redistributed under the terms of the GNU AGPLv3
```

A simple script, that downloads every runner and proxy to a centralised location of the webserver.
Needed, because projects have sometimes a complete different download url, which will cause a much longer codebase for the msmc-cli, since every runner/proxy needs to be filtered and then handeled separately.

## Usage
```
usage: noveria-dl-manager [operation]
operations:
        noveria-dl-manager {-h --help}
        noveria-dl-manager {-V --version}
        noveria-dl-manager {-v --verbose}
        noveria-dl-manager {-n --dry-run}
```

## Dependencies:
* zsh
* jq